#!/usr/bin/env python3

"""
Main PanoptoSync module.
"""

import os
import sys
from argparse import ArgumentParser

import urllib3.exceptions
from pathlib import Path
from typing import Optional

import core
from logs import logger, errlog
import args as module_args

ARGS: module_args.ArgumentsType


def main(argv) -> Optional[int]:
    from core import signal_workers_to_stop
    from input import Credentials

    global ARGS

    parser = module_args.make_command_parser()
    if len(argv) == 1:
        parser.print_help(sys.stderr)
        return 1
    ARGS = parser.parse_args(argv[1:])
    logger.verbosity = ARGS.verb_flags
    ARGS.id_path = Path(ARGS.id_path)

    actions = {
        module_args.ACT_GENID: lambda: Credentials.generate(ARGS.id_path),
        module_args.ACT_SHOWID: lambda: Credentials.show(ARGS.id_path),
        module_args.ACT_SYNC: syncronize,
        module_args.ACT_SINGLE: lambda: linkhandler(parser),
    }

    def check_errors():
        if core.TOTAL_ERRORS > 0:
            errlog()
            errlog(f"Warning: {core.TOTAL_ERRORS} errors - please report them.")
            errlog(f'(check latest files in "{core.LOGDIR}" folder)')

    def close(msg=None):
        signal_workers_to_stop()
        if msg:
            errlog()
            errlog(msg)
        check_errors()

    try:
        #raise Exception("test")
        return actions[ARGS.action]()
    except EOFError:
        close("EOF reached.")
        return 1
    except (KeyboardInterrupt, urllib3.exceptions.MaxRetryError):
        close("Interrupt received.")
        return 1
    except BrokenPipeError:
        # https://docs.python.org/3/library/signal.html#note-on-sigpipe
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        return 1
    except BaseException as unk:
        core.error_to_file()
        close()
        raise unk


def check_missing_download_softwares() -> bool:
    import download
    if not download.MISSING_SOFTWARE:
        return False
    errlog(
        "Unable to find some softwares.",
        "Please install and make them visible from the system path.",
        sep="\n",
    )
    with logger:
        errlog(", ".join(download.MISSING_SOFTWARE))
    return True


def syncronize() -> Optional[int]:
    global ARGS
    import sync
    ARGS = core.asif(ARGS, module_args.SyncArgumentsType)

    if not ARGS.only_rename and check_missing_download_softwares():
        return 1

    # arguments of sync parser
    ARGS.cookies_path = Path(ARGS.cookies_path)
    ARGS.sync_dir = Path(os.path.abspath(ARGS.sync_dir))
    if ARGS.only_rename:
        ARGS.no_search = True
    if ARGS.only_meta:
        ARGS.force_meta = True

    sync.ARGS = ARGS
    with sync.Synchronizer() as s:
        if not s.run():
            return 1


def linkhandler(parser: ArgumentParser) -> Optional[int]:
    global ARGS
    import single
    ARGS = core.asif(ARGS, module_args.SingleArgumentsType)

    if check_missing_download_softwares():
        return 1

    if not ARGS.readstdin and not ARGS.links:
        parser.print_help(sys.stderr)
        return 1

    ARGS.output_dir = Path(os.path.abspath(ARGS.output_dir))
    tmax = min(os.cpu_count(), 3)
    if ARGS.workers > tmax:
        ARGS.workers = tmax
        errlog("Number of workers has been limited to", tmax)

    single.ARGS = ARGS
    with single.LinksHandler() as l:
        if not l.run():
            return 1


if __name__ == "__main__":
    sys.exit(main(sys.argv) or 0)
