import re
import urllib.parse as ulp
from dataclasses import dataclass
from typing import Optional, List

import requests as reqs

from core import Loadable, exitnow
from input import Credentials
from panopto import Panopto
from moodle import Moodle

from web import (
    Cookies, FAKE_USERAGENT, SessionEx, AuthProcedure, AbstractSSOAuth, HTMLIter
)
from logs import Verb, log, errlog


@dataclass
class AAPAuth(AbstractSSOAuth):
    domain: str
    credentials: Credentials
    LABEL = "AAP"
    AUTH_PATH1 = "/oam/server/obrareq.cgi"
    AUTH_PATH2 = "/oamfed/idp/samlv20"
    SUBMIT_PATH = "/oam/server/auth_cred_submit"
    SUBMIT_METHOD = "post"


    def _phase2_handler(self, page: reqs.Response) -> str:
        """SAMLv2 POST redirect phase"""
        # no need to parse twice, <form> will contain <input>s

        form = next(HTMLIter(page.text).search('form', method=None, action=None))

        iinputs = form.search1('input', type=None, name=None, value=None)
        inputs = {t['name']: t['value'] for t in iinputs}

        met = form['method'].lower()
        kw = {('params' if met == 'get' else 'data'): inputs}
        with self.session.request(
            method=met,
            url=form['action'],
            strict200=False,
            **kw
        ) as response:
            return response.next.url

    def _phase1_handler(self, page: reqs.Response) -> str:
        """Credentials input phase"""
        # form with code
        req_id = next(HTMLIter(page.text).search(
            'input', type=None, name="request_id", value=None
        ))['value']

        # post
        content = dict(
            form_username=self.credentials.username,
            form_password=self.credentials.password,
            request_id=req_id,
            form_spidprovider="nospid",
        )
        met = self.SUBMIT_METHOD.lower()
        kw = {('params' if met == 'get' else 'data'): content}
        with self.session.request(
            method=met,
            url=self.urljoin(self.SUBMIT_PATH),
            strict200=False,
            **kw
        ) as response:
            # must not leave with-block,
            # otherwise the connection will be closed without downloading HTML
            if not response.is_redirect:
                redir = self._run_phase(response)
                if redir:
                    return redir
            return response.next.url

    def _run_phase(self, page: reqs.Response) -> Optional[str]:
        if page.is_redirect:
            return page.next.url

        rparts = ulp.urlparse(page.url)
        if self.domain not in rparts.netloc or "text/html" not in page.headers["content-type"]:
            return None
        head = page.text[:2000]

        if ".woff2" in head:
            return self._phase1_handler(page)

        if "hijacking" in head and "document.forms[0].submit()" in head:
            return self._phase2_handler(page)

        return None

    def login_portal(self, redirect: str) -> str:
        """
        :param redirect: Target URL returned by other platforms
        :return: New target URL returned by AAP
        """
        with self.session.get(redirect, strict200=False) as response:
            redir = self._run_phase(response)
            if not redir:
                raise RuntimeError("Unknown authorization phase.")
            return redir

    def authorize(self, redir: str = None) -> bool:
        raise RuntimeError("Platform does not support direct authorization.")


@dataclass
class IntranetAuth(AuthProcedure):
    domain: str
    aap_auth: AAPAuth
    LABEL = "Intranet"
    TEST_PATH = "/html/themes/classic/images/spacer.png"
    AUTH_PATH = "/obrar.cgi"

    def authorize(self, redir: str = None) -> bool:
        if not redir:
            chk, redir = self.is_authorized()
            if chk:
                return True
        redir = self.aap_auth.login_portal(redir)
        rparts = ulp.urlparse(redir)
        assert self.domain in rparts.netloc and rparts.path == self.AUTH_PATH

        # unroll simple redirects
        with self.session.get(redir, allow_redirects=True):
            pass
        return self.is_authorized()[0]


class UniVRBot(Loadable):
    # DRIVER_LOG = 'geckodriver.log'
    AAP_DOMAIN = "aap.univr.it"
    INTRANET_DOMAIN = "intranet.univr.it"
    MOODLE_DOMAIN = "moodledidattica.univr.it"
    PANOPTO_DOMAIN = "univr.cloud.panopto.eu"
    PANOPTO_TUTORIAL_ID = "ef1788d3-46b8-42b9-8f3a-ab6d0116ce35"
    # WARNING: Firefox seems to convert %xx url characters automatically,
    # so space and others chars may be present in a HTML source page.

    RGX_ID = re.compile(r"[?&]id=([^?&/]+)")


    def __init__(self, cred: Credentials, cookies: Cookies = None):
        self.cred = cred
        self.session = SessionEx()
        self.session.headers.update({"User-Agent": FAKE_USERAGENT})
        self.aap_auth = AAPAuth(session=self.session, domain=self.AAP_DOMAIN, credentials=cred)
        self.intranet_auth = IntranetAuth(session=self.session, domain=self.INTRANET_DOMAIN, aap_auth=self.aap_auth)
        self.moodle = Moodle(
            domain=self.MOODLE_DOMAIN, session=self.session, sso_auth=self.aap_auth
        )
        self.panopto = Panopto(
            domain=self.PANOPTO_DOMAIN, session=self.session, sso_auth=self.aap_auth
        )
        self.initial_cookies = cookies

    @property
    def all_cookies(self):
        return self.session.cookies

    def load(self):
        # self.driver.load()
        # if self.initial_cookies is not None:
        #     self.driver.load_cookies(self.initial_cookies)

        ap: AuthProcedure
        for ap in self.intranet_auth, self.moodle.auth, self.panopto.auth:
            log(f"{ap.LABEL}: Logging in...")
            chk, redir = ap.is_authorized()
            if chk:
                log(f"{ap.LABEL}: Already logged in.", erase_prev=True)
            elif not ap.authorize(redir):
                log(f"{ap.LABEL}: Unable to log in.", erase_prev=True)
                exitnow()
            log(f"{ap.LABEL}: Logged successfully.", erase_prev=True)

    def unload(self):
        pass

    # Specific Moodle plugins section

    def get_moodle_panopto_block(self, course_id) -> List[str]:
        with self.session.request(
            method="post",
            url=self.moodle.urls.PANOPTO_BLOCK,
            data=dict(sesskey=self.moodle.auth.sesskey, courseid=course_id)
        ) as response:
            filt = lambda u: u.startswith(self.panopto.urls.BASEURL)
            gen = HTMLIter(response.text).search('a', href=filt)
            maps = (UniVRBot.get_id_from_url(a['href']) for a in gen)
            return [i for i in maps if i is not None]

    @staticmethod
    def get_id_from_url(url: str):
        matches = UniVRBot.RGX_ID.findall(url)
        if not matches:
            return None
        lid = matches[0]
        if lid == UniVRBot.PANOPTO_TUTORIAL_ID:
            return None
        return lid

    def get_panopto_ids_from_course_id(self, course_id):
        def chk(url: str):
            c = url.startswith(self.panopto.urls.VIEWER_PAGE)
            if c: errlog("link is panopto", verbos=Verb.scraping)
            return c

        ret = self.moodle.get_links_from_course_id(course_id, checker=chk, mapper=UniVRBot.get_id_from_url)
        ret += self.get_moodle_panopto_block(course_id)

        return ret
