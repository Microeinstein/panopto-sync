import os
import re
import uuid
from sys import stdin
from dataclasses import dataclass, field
from pathlib import Path
import urllib.parse as ulp
from functools import reduce
from concurrent.futures import ThreadPoolExecutor
from typing import List, Dict, Tuple, Iterable, Optional

from colorama import Fore, Style

from logs import Log, logger, log, errlog
from core import (
    ensure_directory,
    set_file_times,
    criticalsection,
    shutting_down,
    flatten,
    is_debugging,
    makesure,
    error_to_file,
    Loadable,
)
from title_format import ORIGINAL_FILENAME_FMT, format_lesson_filename, pathify_course_name
from content import Lesson
from input import Credentials
from univr import UniVRBot
from panopto import PanoptoError, Panopto
from download import LessonDownloader
from web import try_except_network_error
from progress_bar import CustomMixedBar


from args import SingleArgumentsType

ARGS: SingleArgumentsType  # value is set in module panoptoSync.py


@dataclass
class LinkInfo:
    RGX_LESSON_NUMBER = re.compile(r"^(\S+)\s+#\s+(.+)$")
    RGX_FRAGMENT = re.compile(r"^([^=\"']+)=([\"']?)([^=\"']+)\2$")
    PANOPTO_DUMMY = Panopto(
        domain=UniVRBot.PANOPTO_DOMAIN, session=None, sso_auth=None  # noqa
    )

    uuid: str
    uuid8: str = field(init=False)
    is_folder: bool
    path: Path = field(init=False)
    cust_label: Optional[str]
    label_at_end: bool

    def __post_init__(self):
        self.uuid8 = self.uuid[:8]
        self.path = Path('.')

    @classmethod
    def from_argument(cls, raw: str) -> "LinkInfo":
        label = None
        label_at_end = False
        _m = cls.RGX_LESSON_NUMBER.match(raw)
        if _m:
            raw, label = _m.groups()
            label_at_end = label.endswith("*")
            if label_at_end:
                label = label[:-2]
            if label.startswith("*"):
                label = label[1:]

        # check if link
        if raw.startswith("https://") or raw.startswith("http://"):
            url = ulp.urlparse(raw)

            if cls.PANOPTO_DUMMY.urls.VIEWER_PAGE in raw:
                url_query: Dict[str] = ulp.parse_qs(url.query)  # noqa
                if 'id' in url_query and url_query['id']:
                    return LinkInfo(
                        uuid=url_query['id'][0],
                        is_folder=False,
                        cust_label=label,
                        label_at_end=label_at_end
                    )

            if cls.PANOPTO_DUMMY.urls.SESSIONS_LIST in raw:
                mtc = cls.RGX_FRAGMENT.match(ulp.unquote(url.fragment))
                if mtc and mtc.group(1) == 'folderID':
                    return LinkInfo(
                        uuid=mtc.group(3),
                        is_folder=True,
                        cust_label=label,
                        label_at_end=label_at_end
                    )

            raise ValueError(f"Invalid url: '{url}'")

        # check if uuid
        try:
            folder = raw.endswith('/')
            if folder:
                raw = raw[:-1]
            uuid.UUID(raw)  # unused object, but throws exception if not uuid
            # link_made_by_uuid = f"{cls.PANOPTO_DUMMY.urls.VIEWER_PAGE}?id={lesson_id}"
            return LinkInfo(raw, folder, label, label_at_end)
        except ValueError:
            pass  # not an uuid

        # unknown user input
        raise ValueError(f"Invalid link: '{raw}'")


# noinspection PyMethodMayBeStatic
class LinksHandler(Loadable):
    def __init__(self):
        self._bot: Optional[UniVRBot] = None
        self.folders = None
        self.num_failed_lessons = None
        self.cred = None
        self.credkey = None

    @property
    def bot(self):
        if self._bot is None:
            self._bot = UniVRBot(self.cred)
            self._bot.load()
        return self._bot

    @criticalsection
    def _reserve_filename(self, legger: Log, info: LinkInfo, lesson: Lesson) -> bool:
        fname = format_lesson_filename(ORIGINAL_FILENAME_FMT, "", lesson.plain())
        custlbl = info.cust_label

        if not custlbl:
            lesson.filename = fname
        elif custlbl.isnumeric():
            a = f"{fname} ({custlbl})"  # lesson (12)
            b = f"{custlbl}. {fname}"  # 12. lesson
            lesson.filename = a if info.label_at_end else b
        else:
            a = f"{fname} {custlbl}"  # lesson suffix
            b = f"{custlbl} {fname}"  # prefix lesson
            lesson.filename = a if info.label_at_end else b

        tmp_fname: str
        for suffix in ["", info.uuid8]:
            tmp_fname = lesson.filename
            if suffix:
                tmp_fname += f" ({suffix})"
            tmp_fname += LessonDownloader.OUTPUT_EXT

            dest_dir: Path = ARGS.output_dir / info.path
            fullpath: Path = dest_dir / tmp_fname
            ensure_directory(dest_dir)
            if not fullpath.exists():
                fullpath.touch(exist_ok=False)
                break
            if ARGS.overwrite:
                legger.out("Already existing; overwriting...")
                break
        else:  # all filenames exist
            if not ARGS.overwrite:
                legger.err("Already existing; skipping...")
                self.num_failed_lessons += 1
                return False
            raise RuntimeError("Unreachable code.")

        lesson.filename = tmp_fname
        return True

    def _handle_single(self, info: LinkInfo):
        legger = Log()
        legger.prefix = f"[{info.uuid8}]"
        shutting_down()

        try:
            lesson: Lesson = self.bot.panopto.get_lesson_data(info.uuid)
        except PanoptoError:
            legger.err(f"{Fore.RED}Cannot retrieve info{Style.RESET_ALL}")
            self.num_failed_lessons += 1
            return

        if not self._reserve_filename(legger, info, lesson):
            return
        dest_dir = ARGS.output_dir / info.path
        lesson_path = dest_dir / lesson.filename
        legger.out("File:", info.path / lesson.filename)

        if lesson_path.is_dir():
            legger.err("Warning: the output path is blocked by a directory")
            self.num_failed_lessons += 1
            return
        makesure(lambda: lesson.meaningful_streams is not None,
                 f"[{info.uuid8}] Delivery info should have been retrieved earlier")

        shutting_down()
        if lesson.meaningful_streams:
            with LessonDownloader(lesson, dest_dir, self.bot.session, parallel=ARGS.workers) as d:
                d.run()
            if ARGS.workers == 1:
                log()
        else:
            legger.err("Warning: this lesson has no streams, ignoring...")
            self.num_failed_lessons += 1
            return

        if lesson_path.is_file():
            set_file_times(lesson_path, lesson.date)
        else:
            legger.err("Warning: missing file.")
            self.num_failed_lessons += 1

    def _handle_serial(self, infos: List[LinkInfo]):
        for i in infos:
            try:
                self._handle_single(i)
            except Exception as ex:  # noqa
                if is_debugging():
                    raise ex
                error_to_file()
                self.num_failed_lessons += 1

    def _handle_parallel(self, infos: List[LinkInfo]):
        with CustomMixedBar(message=CustomMixedBar.make_msg("#download")) as prog:
            prog.steps_mode_init(len(infos))
            prog.suffix = "%(index)d/%(max)d %(elapsed_t)s"

            def worker(i):
                if shutting_down(False):
                    return
                self._handle_serial([i])
                prog.steps_hook()

            # `with` statement will `executor.shutdown(wait=True)`
            with ThreadPoolExecutor(max_workers=ARGS.workers) as executor:
                executor.map(worker, infos)
            log()

    def _link_mapper(self, raw: str, check_files: bool) -> Iterable[LinkInfo]:
        raw = raw.strip()
        if not raw or raw[0] in ('#', ';'):
            return

        if check_files and os.path.isfile(raw):
            with open(raw) as file:
                for line in file:
                    yield from self._link_mapper(line, False)
                return

        try:
            # `raw` can be a link  or a lesson id
            yield LinkInfo.from_argument(raw)
            return
        except ValueError as ex:
            errlog(*ex.args)
        self.num_failed_lessons += 1

    def _folder_solver(self, link: LinkInfo):
        # get ALL folders (will load bot)
        self.folders = self.folders or self.bot.panopto.get_folders()
        if link.uuid not in self.folders:
            errlog("Folder is missing, are you subscribed?  " + link.uuid)
            self.num_failed_lessons += 1
            return

        # get lessons per path
        for path, lessons in self.bot.panopto.get_folder_lessons_ids(link.uuid):
            # convert path from IDs to names
            _id2path = lambda fid: Path(pathify_course_name(self.folders[fid]['Name']))
            path2: Path = reduce(lambda a, b: a / b, map(_id2path, path))

            if not lessons:
                log(f"Folder is empty: '{str(path2)}'")
                continue

            # apply folder information
            for link2 in self._link_seq_mapper(lessons, False):
                link2.path = path2
                link2.cust_label = link.cust_label
                link2.label_at_end = link.label_at_end
                yield link2
                # break  # only 1 lesson per folder, for debug

    def _link_seq_mapper(self, source: Iterable[str], check_files: bool):
        for raw in source:
            for link in flatten(self._link_mapper(raw, check_files)):
                if not link.is_folder:
                    yield link
                    continue
                yield from self._folder_solver(link)

    def load(self):
        self.cred, self.credkey = Credentials.read(ARGS.id_path)
        ensure_directory(ARGS.output_dir)
        self.num_failed_lessons = 0

    def _handle_source(self, source: Iterable[str]) -> Tuple[bool, bool]:
        infos = list(self._link_seq_mapper(source, True))
        if not infos:
            return False, False  # fail before starting

        makesure(
            lambda: next(filter(lambda l: l.is_folder, infos), None) is None,
            "Some links are still folders! Should have been processed..."
        )
        _ = self.bot  # load bot

        log("Downloading", len(infos), "lessons...")
        handler = self._handle_parallel if ARGS.workers > 1 else self._handle_serial
        run_ok, _ = try_except_network_error(handler, infos)
        if not run_ok:
            # network error
            return False, True
        logger.reset_level()
        log("Done.")

        return True, True

    def run(self) -> bool:
        ok = True
        handled = False

        def proc(links):
            nonlocal ok, handled
            ok2, handled2 = self._handle_source(links)
            ok = ok and ok2
            handled = handled or handled2

        if ARGS.readstdin:
            for line in stdin:
                proc([line.strip()])

        proc(ARGS.links)

        if not handled:
            log("No lessons to download...")
        else:
            # mimic sync behavior, do not fail with download errors
            if self.num_failed_lessons > 0:
                errlog("Warning: unable to elaborate", self.num_failed_lessons, "lessons.")

        return ok

    def unload(self):
        if self._bot is not None:
            self._bot.unload()
        self._bot = None
        self.cred = None
        self.credkey = None
