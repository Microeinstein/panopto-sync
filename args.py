import argparse
import re
from pathlib import Path
from typing import List, Union, Protocol

from logs import Verb, errlog
from core import makenames


@makenames
class ArgumentsType(Protocol):
    id_path: Union[str, Path]
    verb_flags: List[Verb]


@makenames
class SyncArgumentsType(ArgumentsType):
    cookies_path: Union[str, Path]
    sync_dir: Union[str, Path]
    force_older: bool
    force_existing: bool
    no_search: bool
    dry_run: bool
    known_courses: bool
    unknown_courses: bool
    courses_filters: List[re.Pattern]
    force_meta: bool
    only_meta: bool
    only_rename: bool


@makenames
class SingleArgumentsType(ArgumentsType):
    output_dir: Union[str, Path]
    workers: int
    overwrite: bool
    readstdin: bool
    links: List[str]



ACT_GENID = "genid"
ACT_SHOWID = "showid"
ACT_SYNC = "sync"
ACT_SINGLE = "single"


def make_command_parser():
    _mfc = lambda prog: argparse.RawTextHelpFormatter(prog, max_help_position=29)

    def verb_flags(names):
        return [Verb[n] if n in Verb.__members__ else (
            errlog("Known flags:", ', '.join(Verb.__members__.keys()), "\n"),
            parser.error(f"{n} is unknown.")
        ) for n in names.split(',')]

    parser = argparse.ArgumentParser(  # noqa
        description="""\
Automatically downloads and synchronizes UniVR Panopto lessons.

To show additional arguments, specify an action (eg. sync -h)""",
        formatter_class=_mfc,
    )
    subparsers = parser.add_subparsers(metavar="ACTION", dest="action", prog=None, required=True)  # noqa

    parser.add_argument(
        "--idfile",
        "-i",
        metavar="PATH",
        dest=ArgumentsType.K_ID_PATH,
        default="default.id",
        help="credentials file",
    )

    parser.add_argument(
        "--verbose",
        "-v",
        dest=ArgumentsType.K_VERB_FLAGS,
        type=verb_flags,
        default=[],
        help="set verbosity flags (comma separated)",
    )

    def add_genid():
        subparsers.add_parser(ACT_GENID, formatter_class=_mfc, help="create credentials file")

    def add_showid():
        subparsers.add_parser(ACT_SHOWID, formatter_class=_mfc, help="show credentials file content")

    def add_sync():
        snc = subparsers.add_parser(ACT_SYNC, formatter_class=_mfc, help="synchronize the lessons")
        snc.add_argument(
            "--cookies",
            "-c",
            metavar="PATH",
            dest=SyncArgumentsType.K_COOKIES_PATH,
            default="default.cookies",
            help="browser cookies database file",
        )
        snc.add_argument(
            "--syncdir",
            "-d",
            metavar="PATH",
            dest=SyncArgumentsType.K_SYNC_DIR,
            default="lessons",
            help="synchronization directory",
        )
        snc.add_argument(
            "--force-older",
            "-w",
            dest=SyncArgumentsType.K_FORCE_OLDER,
            action="store_true",
            help="do not abort if using an older download algorithm",
        )
        snc.add_argument(
            "--force-existing",
            "-e",
            dest=SyncArgumentsType.K_FORCE_EXISTING,
            action="store_true",
            help="download a lesson even if a video already exists",
        )
        snc.add_argument(
            "--no-search",
            "-n",
            dest=SyncArgumentsType.K_NO_SEARCH,
            action="store_true",
            help="do not search new courses or lessons - use cache",
        )
        snc.add_argument(
            "--dry-run",
            "-t",
            dest=SyncArgumentsType.K_DRY_RUN,
            action="store_true",
            help="do not save/download/change anything on disk",
        )

        knowledge_group = snc.add_mutually_exclusive_group()
        knowledge_group.add_argument(
            "--known-courses",
            "-k",
            dest=SyncArgumentsType.K_KNOWN_COURSES,
            action="store_true",
            help="search new lessons only for known courses",
        )
        knowledge_group.add_argument(
            "--unknown-courses",
            "-u",
            dest=SyncArgumentsType.K_UNKNOWN_COURSES,
            action="store_true",
            help="search new lessons only for unknown courses",
        )
        knowledge_group.add_argument(
            "--courses",
            metavar="FILTER",
            dest=SyncArgumentsType.K_COURSES_FILTERS,
            nargs="+",
            type=lambda query: re.compile(query, re.IGNORECASE),
            help="only consider courses containing one of these filters (regex)",
        )

        download_group = snc.add_mutually_exclusive_group()
        download_group.add_argument(
            "--force-meta",
            "-m",
            dest=SyncArgumentsType.K_FORCE_META,
            action="store_true",
            help="retrieve lesson metadata even if cached",
        )
        download_group.add_argument(
            "--only-meta",
            "-M",
            dest=SyncArgumentsType.K_ONLY_META,
            action="store_true",
            help="do not download media streams; update metadata",
        )
        download_group.add_argument(
            "--only-rename",
            "-r",
            dest=SyncArgumentsType.K_ONLY_RENAME,
            action="store_true",
            help="download nothing; update filenames (implies -n)",
        )

    def add_single():
        sin = subparsers.add_parser(ACT_SINGLE, formatter_class=_mfc, help="download lessons from UniVR Panopto given a link or a file")
        sin.add_argument(
            "--output",
            "-d",
            metavar="PATH",
            dest=SingleArgumentsType.K_OUTPUT_DIR,
            default="lessons",
            help="output directory",
        )
        sin.add_argument(
            "--parallel",
            "-p",
            default=1,
            metavar="WORKERS",
            dest=SingleArgumentsType.K_WORKERS,
            type=int,
            help="number of parallel download workers",
        )
        sin.add_argument(
            "--overwrite",
            "-O",
            dest=SingleArgumentsType.K_OVERWRITE,
            action="store_true",
            help="download a lesson even if a video already exists",
        )
        sin.add_argument(
            '-',
            dest=SingleArgumentsType.K_READSTDIN,
            help=argparse.SUPPRESS,
            action='store_true',
        )
        sin.add_argument(
            "links",
            metavar="LINKS",
            default=[],
            # dest=SingleArgumentsType.K_LESSONS,
            nargs="*",
            help="links, IDs or files containing a list of links (one per line)",
        )

    add_genid()
    add_showid()
    add_sync()
    add_single()

    return parser
