import os
import re
import tempfile
from itertools import chain
from pathlib import Path
from http.client import HTTPException
from typing import List, Dict, Set, Iterable, Optional, Callable

from logs import Verb, logger, log, errlog
from core import (
    ensure_directory,
    walk_no_recur_symlinks,
    get_mkv_title,
    get_file_time,
    set_file_times,
    group_by,
    is_debugging,
    makesure,
    error_to_file,
    Loadable,
    take_unique_with_order,
)
from web import try_except_network_error
from title_format import format_lesson_filename, deduplicate_names
from content import Course, Lesson, Options
from cache import Cache
from input import Credentials
from univr import UniVRBot
from moodle import MoodleUnsubscribedError
from panopto import PanoptoError
from download import LessonDownloader


from args import SyncArgumentsType

ARGS: SyncArgumentsType  # value is set in module panoptoSync.py


# noinspection PyMethodMayBeStatic
class Synchronizer(Loadable):
    def __init__(self):
        self.cache: Optional[Cache] = None
        self._bot: Optional[UniVRBot] = None
        self.num_failed_courses = None
        self.num_failed_lessons = None
        self.cred = None
        self.credkey = None
        # self.old_curdir = None

    @property
    def bot(self):
        if self._bot is None:
            self._bot = UniVRBot(self.cred)
            self._bot.load()
        return self._bot

    # [SINGLE LESSON]
    def _handle_lesson(self, course: Course, lesson: Lesson):
        log(lesson.name, pointer=True)
        with logger:
            lpath = course.resolve(lesson)
            log("File:", lesson.filename)

            def download():
                if lpath.is_dir():
                    errlog("Warning: the output path is blocked by a directory.")
                    self.num_failed_lessons += 1
                    return
                makesure(
                    lambda: lesson.meaningful_streams is not None, "Delivery info should have been retrieved earlier"
                )
                if lesson.meaningful_streams:
                    with LessonDownloader(lesson, course.dir, self.bot.session, dryrun=ARGS.dry_run) as d:
                        d.run()
                else:
                    errlog("Warning: this lesson has no streams, ignoring...")
                    self.num_failed_lessons += 1
                    return

            if not (ARGS.only_meta or ARGS.only_rename):
                lexists = lpath.exists()
                if not lexists or ARGS.force_existing:
                    if lexists:
                        log("Already existing; overwriting...")
                    download()
                    log()

            # in case of failure, do not cache lesson (returned earlier)
            course.lessons.deep_set(lesson)

            # set file access/modified times
            if lpath.is_file():
                if not ARGS.dry_run:
                    set_file_times(lpath, lesson.date)
            else:
                errlog("Warning: missing file.")
                self.num_failed_lessons += 1

    # [LESSONS GROUP]
    def _rename_new_filenames(self, course: Course, all_lessons: List[Lesson]):
        if not course.dir.exists() or ARGS.dry_run:
            for lesson in all_lessons:
                lesson.filename = lesson.filename_new
            return

        # filename_new can still collide with current filename of one lesson
        tmpdir = tempfile.mkdtemp(dir=course.dir)

        # handle missing lessons too to avoid collisions
        # (move all lessons to a temporary folder, with temporary names)
        for lesson in all_lessons:
            if not lesson.filename:
                continue
            lsrc = course.resolve(lesson)
            # it is certain that the temporary destination does not exists
            handle, tmpdest_abs = tempfile.mkstemp(dir=tmpdir, suffix=LessonDownloader.OUTPUT_EXT)
            tmpdest_rel = str(Path(tmpdest_abs).relative_to(course.dir))
            lesson.filename = tmpdest_rel
            os.close(handle)
            # (if lesson exists replace temp file with it, otherwise delete temp file)
            if lsrc.exists():
                lsrc.replace(tmpdest_abs)
            else:
                os.remove(tmpdest_abs)

        for lesson in all_lessons:
            if not lesson.filename:
                lesson.filename = lesson.filename_new
                continue
            lsrc = course.resolve(lesson)
            lesson.filename = lesson.filename_new
            ldest = course.resolve(lesson)
            makesure(lambda: not ldest.exists() or self._trace_lesson((lesson,)), "Absurd collision")
            if lsrc.exists():
                lsrc.rename(ldest)

        # everything run smoothly
        os.rmdir(tmpdir)  # throws exception in case of remaining files

    def _trace_lesson(self, lessons: Iterable[Lesson]):
        if not logger.is_verbose(Verb.content):
            return

        dbgkeys = (
            Lesson.K_NAME,
            Lesson.K_NAME_FIXED,
            Lesson.K_DATE,
            Lesson.K_DATE_NAME,
            Lesson.K_FILENAME,
            Lesson.K_FILENAME_NEW,
        )

        for lesson in lessons:
            for k in dbgkeys:
                errlog(f"{k:12}: {getattr(lesson, k)}")
            errlog()

    def _assign_new_filenames(self, course: Course, all_lessons: List[Lesson]):
        fnnkey = lambda l: l.filename_new.lower()
        datekey = lambda l: l.date_better

        for lesson in all_lessons:
            lesson.filename_new = format_lesson_filename(
                self.cache.options.lessonsFilenameFormat, course.name, lesson.plain()
            )

        for _1, g in group_by(all_lessons, fnnkey):
            grouped: List[Lesson] = sorted(g, key=datekey)
            if len(grouped) == 1:
                continue

            for i, lesson in enumerate(grouped):
                lesson.filename_new = deduplicate_names(lesson.filename_new, i + 1)

            self._trace_lesson(grouped)

        for lesson in all_lessons:
            lesson.filename_new += LessonDownloader.OUTPUT_EXT

        # list(dict.keys()) faster than set()  (maybe?)
        all_new = take_unique_with_order(map(lambda l: l.filename_new, all_lessons))
        # count lessons with the same fn and check == 1
        is_once = lambda fn: sum(1 for _ in filter(lambda l: l.filename_new == fn, all_lessons)) == 1
        collisions = [fn for fn in all_new if not is_once(fn)]
        makesure(lambda: not collisions, "Multiple lessons with the same destination filename")

    def _try_match_lesson_file(
        self,
        copy_known: List[Lesson],
        files_info: Dict,
        extra_files: List,
        absfile: str,
        func: Callable[[int, Lesson], bool],
    ) -> bool:  # noqa
        # [(index, Lesson), ...]
        matched = filter(func, enumerate(copy_known))  # noqa
        # n_les = len(matched)

        il: Optional[int] = None
        lesson: Optional[Lesson] = None
        ambiguity = False
        try:
            il, lesson = next(matched)
            next(matched)  # more than 1 match
        except StopIteration:
            pass
        else:
            ambiguity = True

        if lesson is None:  # no matches
            extra_files.append(absfile)
            return True
        if not ambiguity:
            lfixed = os.path.basename(absfile)
            if lesson.filename != lfixed:
                log(lesson.filename, pointer=True)
                with logger:
                    log(lfixed)
            lesson.filename = lfixed
            del files_info[absfile]
            del copy_known[il]
            return True
        return False

    def _check_course_integrity(self, course: Course):
        # fix associations between cached Lesson entries and files
        log("Checking course integrity...")

        copy_known: List[Lesson] = list(course.lessons.values())
        files_info = dict()
        extra_files = list()

        with logger:
            if not course.dir.exists():
                log("(no directory)")
            else:
                log("Populating files...")

                # recover temporary names
                for abscur, dirs, _ in walk_no_recur_symlinks(course.dir):
                    not_temp = filter(lambda in_di: not in_di[1].startswith("tmp"), enumerate(dirs))
                    not_temp = sorted(not_temp, key=lambda t: t[0], reverse=True)  # sort by index descending
                    for i, d in not_temp:
                        del dirs[i]
                    for d in dirs:
                        for abscur2, _, files in walk_no_recur_symlinks(Path(abscur, d)):
                            for fi in files:
                                lsrc = Path(abscur2, fi)
                                ldest = Path(course.dir, fi)
                                makesure(lambda: not ldest.exists(), "Absurd collision")
                                lsrc.rename(ldest)
                            os.rmdir(Path(abscur2))  # raises if directory is not empty
                    break

                # scan lessons title and modification date
                rgx = LessonDownloader.RGX_PARTIAL_STREAM
                for abscur, dirs, files in walk_no_recur_symlinks(course.dir):
                    internal = filter(lambda in_di: rgx.search(in_di[1]), enumerate(dirs))
                    internal = sorted(internal, key=lambda t: t[0], reverse=True)  # sort by index descending
                    for i, d in internal:
                        del dirs[i]

                    for fi in files:
                        absfile = os.path.join(abscur, fi)
                        fi = get_mkv_title(absfile)
                        if not fi:
                            continue
                        # ignore total microseconds to avoid different time resolutions, and so false negatives
                        da = get_file_time(absfile).replace(microsecond=0)
                        files_info[absfile] = (fi, da)

                log("Rematching lessons and files together...")
                for absfile, (title, date) in files_info.copy().items():
                    # [title] n_les > 1
                    #       caso:  nome originale è uguale [esempio: corso python]
                    # assunzione:  non può esserci la stessa data sul nome di più lezioni
                    #    implica:  la data di modifica non può essere stata estratta dal nome stesso ("date_name")
                    #                  ma deve provenire dai metadati
                    #       idea:  cerco di riassociare lezioni e files in base alla data di modifica

                    matches = lambda f: self._try_match_lesson_file(copy_known, files_info, extra_files, absfile, f)
                    tests = (
                        lambda il: il[1].name == title,
                        lambda il: il[1].date.replace(microsecond=0) == date,
                    )
                    if any(map(matches, tests)):
                        continue

            if extra_files:
                errlog("Warning, extra files:")
                with logger:
                    for ef in extra_files:
                        errlog(os.path.basename(ef), pointer=True)
                errlog()

            if copy_known:
                errlog("Warning, missing files or extra lessons:")
                with logger:
                    for le in copy_known:
                        errlog(le.name, f"({le.date})", pointer=True)
                errlog()

            if not (extra_files or copy_known):
                log("Aligned.")

    # [SINGLE COURSE]
    def _get_panopto_lesson_data(self, course: Course, lesson_id: str) -> Optional[Lesson]:
        # log('Getting data...')
        is_known = lesson_id in course.lessons
        lknown = None
        if is_known:
            lknown = course.lessons[lesson_id]
            makesure(lambda: not lknown.is_raw, "Trying to get panopto data of raw lesson")

        # course.lessons.setdefault(lesson_id, {})  # add initial placeholder
        try:
            return self.bot.panopto.get_lesson_data(lesson_id)
        except HTTPException:
            err_msg = "Unable to get data for lesson"
        except PanoptoError as pe:
            errcode_name = re.sub(r"(\w)([A-Z])", r"\1 \2", pe.code.name).lower()
            err_msg = f"Unable to access the lesson ({errcode_name})"

        # log(end='', erase=True)
        lurl = f"{self.bot.panopto.urls.VIEWER_PAGE}?id={lesson_id}"
        lname = lknown.name if (is_known and lknown.name is not None) else lurl
        errlog(lname, pointer=True)
        with logger:
            errlog(err_msg)
        self.num_failed_lessons += 1
        return None

    def _retrieve_panopto_lesson_entry(self, course: Course, lesson_id: str):
        cached_lesson = course.lessons.get(lesson_id)
        if cached_lesson:
            if cached_lesson.is_raw:  # must not retrieve panopto data
                return cached_lesson
            lexists = course.resolve(cached_lesson).exists()
            if lexists and not ARGS.force_meta:
                return cached_lesson
        full_data = self._get_panopto_lesson_data(course, lesson_id)
        # return old data in case of failure (example: lesson is removed)
        return full_data or cached_lesson

    def _handle_course(self, course: Course, save_to_cache: bool):
        log(course.name, pointer=True)

        if ARGS.courses_filters is not None:
            matcher = (r.search(course.name) for r in ARGS.courses_filters)
            has_match = next(filter(lambda m: m is not None, matcher), None)
            if not has_match:
                with logger:
                    log("(ignored by filters)")
                    log()
                return
        if save_to_cache:
            self.cache.courses.deep_set(course)
        # skip courses that certainly have 0 lessons
        if not ARGS.only_rename and (course.skip or (course.year is not None and course.year < 2019)):
            return
        old_failed = self.num_failed_lessons
        course.assign_dir(ARGS.sync_dir)

        with logger:
            self._check_course_integrity(course)

            try:
                ids: Iterable[str] = course.lessons.keys()
                newids: Set[str] = set()
                if not ARGS.no_search:
                    panids = self.bot.get_panopto_ids_from_course_id(course.id)
                    newids = set(panids) - set(ids)
                    ids = take_unique_with_order(chain(ids, panids))
                # try getting full data for unknown or missing lessons;
                # None = unknown lesson & panopto returned the ID but does not provide data for it (locked lesson)
                all_lessons = list(filter(None, map(lambda i: self._retrieve_panopto_lesson_entry(course, i), ids)))
                if not ARGS.no_search and course.search_raws:
                    all_lessons.extend(self.bot.moodle.get_all_raw_lessons_data(course.id))

                # queue must contain every lesson with cached or full data;
                # ignore missing data [filtered-out Nones] only for previously unknown lessons;
                # remove duplicated entries for raw-lessons
                queue_ids = take_unique_with_order(map(lambda l: l.id, all_lessons))
                makesure(
                    lambda: all(i in queue_ids or i in newids for i in ids),
                    "Initial queue is not a subset of final queue",
                )
                self._assign_new_filenames(course, all_lessons)
                self._rename_new_filenames(course, all_lessons)

                datekey = lambda l: l.date_better
                for lesson in sorted(all_lessons, key=datekey):
                    try:
                        self._handle_lesson(course, lesson)
                    except Exception as ex:  # noqa
                        if is_debugging():
                            raise ex
                        error_to_file()
                        self.num_failed_lessons += 1

                total_lessons = len(course.lessons)
                if total_lessons > 0:
                    cur_failed = self.num_failed_lessons - old_failed
                    if not (ARGS.only_meta or ARGS.only_rename):
                        log(f"(completed {total_lessons - cur_failed}/{total_lessons})")
                else:
                    log("(no lessons)")

            except MoodleUnsubscribedError:
                errlog("Warning: not subscribed to this course.")
                self.num_failed_courses += 1

            log()

    # [ITERATE COURSES]
    def _find_new_courses(self) -> List[Course]:
        c_all = self.bot.moodle.get_all_courses()
        return list(filter(lambda c: c.id not in self.cache.courses, c_all))

    def _sync_courses(self):
        def common(getter: Callable, save_to_cache: bool):
            with logger:
                courses = getter()
                if not courses:
                    log("(no courses)")
                    return
                for course in courses:
                    self._handle_course(course, save_to_cache)

        def from_cache():
            return self.cache.courses.values()

        if ARGS.no_search:
            msg = "Downloading known lessons for known courses..."
            if ARGS.only_rename:
                msg = "Renaming known lessons..."
            log(msg)
            common(from_cache, False)
            return

        if not ARGS.unknown_courses:
            numc = " specified " if ARGS.courses_filters else " "
            log(f"Finding lessons for known{numc}courses...")
            common(from_cache, False)

        if not ARGS.known_courses:
            log("Finding new courses...")
            common(self._find_new_courses, True)

    # [LOAD-UNLOAD MECHANISM]
    def load(self):
        self.cred, self.credkey = Credentials.read(ARGS.id_path)
        # self.cookies = read_cookies(ARGS.cookies_path, self.credkey)
        ensure_directory(ARGS.sync_dir)
        # can't do this
        # self.old_curdir = os.path.abspath(os.curdir)
        # os.chdir(ARGS.sync_dir)
        self.cache = Cache.read(ARGS.sync_dir)
        self.num_failed_courses = 0
        self.num_failed_lessons = 0

    def _check_version(self) -> bool:
        if self.cache.courses:
            last_dv = self.cache.options.downloaderVersion
            curr_dv = Options.VERSION
            if last_dv is None or last_dv < curr_dv:
                errlog("Warning: there's a new download algorithm,")
                errlog("some lessons may require redownloading.")
                errlog()
            elif last_dv > curr_dv:
                errlog("Warning: you're using an old download algorithm!")
                if not ARGS.force_older:
                    errlog("Aborting...")
                    return False
                errlog()
            last_dv = last_dv or curr_dv
            set_dv = max(last_dv, curr_dv)
        else:
            set_dv = Options.VERSION
        self.cache.options.downloaderVersion = set_dv
        return True

    def run(self) -> bool:
        if not self._check_version():
            return False
        log("Synchronizing lessons...")

        if ARGS.only_rename:
            self._sync_courses()
        else:
            ok, _1 = try_except_network_error(self._sync_courses)
            if not ok:
                return False

        logger.reset_level()
        log("Done.")
        if self.num_failed_courses > 0:
            errlog("Warning: unable to elaborate", self.num_failed_courses, "courses.")
        if self.num_failed_lessons > 0:
            errlog("Warning: unable to elaborate", self.num_failed_lessons, "lessons.")

        return True

    def unload(self):
        if not ARGS.dry_run and self.cache:
            self.cache.write()
        if self._bot is not None:
            # if not ARGS.dry_run:
            #     write_cookies(ARGS.cookies_path, self.cookies, self.credkey)
            self._bot.unload()
        # os.chdir(self.old_curdir)
        self._bot = None
        self.cred = None
        self.credkey = None
        self.cache = None
