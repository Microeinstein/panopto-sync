import os
import json
import shutil
from pathlib import Path
from dataclasses import dataclass, field

from logs import log
from content import JsonDict, Record, RecordSet, Course, Options


@dataclass
class RawCache(Record):
    options: JsonDict
    courses: JsonDict


@dataclass
class Cache:
    FILENAME = "sync.json"

    path: Path
    options: Options = field(default_factory=Options)
    courses: RecordSet[Course] = field(default_factory=RecordSet)

    @classmethod
    def read(cls, dir_: Path = None):
        log("Reading cache...")
        p = Path(os.path.abspath(dir_ or Path()), Cache.FILENAME)
        if not p.is_file():
            return Cache(p)

        with p.open(mode="r", encoding="utf-8") as fi:
            raw = RawCache.new(json.load(fi))

        # allow None / dataclass-annotations mechanism (safer)
        opts = Options.new(raw.options)
        crs = Course.from_raw_dict(raw.courses)
        return Cache(path=p, options=opts, courses=crs)

    def write(self):
        log("Writing cache...")
        opts = self.options.dump()
        crs = Course.to_raw_dict(self.courses)
        raw = RawCache(options=opts, courses=crs)

        p = self.path
        exists = p.exists()
        bkp = Path(p.parent, f"{Cache.FILENAME}.bkp")
        broken = Path(p.parent, f"{Cache.FILENAME}.broken")
        try:
            if exists:
                shutil.copy(p, bkp)
            with p.open(mode="w", encoding="utf-8") as fo:
                json.dump(raw.plain(), fo, sort_keys=True, indent="\t")
        except TypeError as ex:
            shutil.move(p, broken)
            if exists:
                shutil.copy(bkp, p)
            raise ex
