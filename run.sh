#!/bin/bash


# print gray to stderr without commands
_dbg() ( set +x;  echo -n $'\e[90m';  "$@";  echo -n $'\e[0m'; ) >&2


# self-knowledge
SELF="${BASH_SOURCE[0]}"
REALSELF="$(realpath -s "$SELF")"


# script is sourced?
if [[ "$0" != "$BASH_SOURCE" ]]; then
    # debug
    set -x
else
    # change directory (must exists) without following symlinks
    cd "$(dirname "$REALSELF")" || exit
    _dbg echo "WorkDir: $PWD"
fi

TMP="/run/user/$(id -u)/$$"


# dependencies
DEPS=(bash)
if ! command -V "${DEPS[@]}" &>/dev/null; then  # no output if successful
    command -V "${DEPS[@]}"                     # output if failure
    exit 1
fi


epoch() { date '+%s'; }

print_array() {
    local -n ref="$1"
    printf '[%s] ' "${ref[@]}"
    echo
}


prod() {
    a=("${a[@]}" "$@")
    print_array a
    "${a[@]}"
}


tests() {
    cd tests
    a=("${a[@]}" "$@")
    print_array a
    "${a[@]}"
}


main() {
    # Usage:  [IDFILE=...]  bash run.sh  <prod|tests>  [args...]

    source "/opt/miniconda/etc/profile.d/conda.sh"
    if [[ -d venv ]]; then
        conda activate ./venv
    else
        conda activate "../../Venv/$(basename "$PWD")"
    fi

    #local PROJ="$PWD"
    local a=(
        python "$(realpath -ms panoptoSync.py)"
        --idfile "$(realpath -ms "${IDFILE:-default.id}")"
    )
    "$@"
}


main "$@"
